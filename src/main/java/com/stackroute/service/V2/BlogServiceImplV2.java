package com.stackroute.service.V2;

import com.stackroute.domain.V2.BlogV2;
import com.stackroute.repository.V2.BlogRepositoryV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * This class should implement BlogServiceV2 Interface and override all the methods and provide implementation for all the methods
 */
@Service
public class BlogServiceImplV2 implements BlogServiceV2 {
    private BlogRepositoryV2 blogRepository;

    /**
     * Constructor based Dependency injection to inject BlogRepository here
     */
    @Autowired
    public BlogServiceImplV2(BlogRepositoryV2 blogRepository) {
        this.blogRepository = blogRepository;
    }

    /**
     * AbstractMethod to save a blog
     *
     * @param blog
     */
    @Override
    public BlogV2 saveBlog(BlogV2 blog) {
        return blogRepository.save(blog);
    }

    /**
     * AbstractMethod to get all blogs
     */
    @Override
    public List<BlogV2> getAllBlogs() {
        return (List<BlogV2>) blogRepository.findAll();
    }

    /**
     * AbstractMethod to get blog by id
     *
     * @param id
     */
    @Override
    public BlogV2 getBlogById(int id) {
        BlogV2 blog = null;
        blog = blogRepository.findById(id).get();
        return blog;
    }

    /**
     * AbstractMethod to delete blog by id
     *
     * @param id
     */
    @Override
    public BlogV2 deleteBlog(int id) {
        BlogV2 blog = null;
        Optional optional = blogRepository.findById(id);
        if (optional.isPresent()) {
            blog = blogRepository.findById(id).get();
            blogRepository.deleteById(id);
        }
        return blog;
    }

    /**
     * AbstractMethod to update a blog
     *
     * @param blog
     */
    @Override
    public BlogV2 updateBlog(BlogV2 blog) {
        BlogV2 updatedBlog = null;
        Optional optional = blogRepository.findById(blog.getBlogId());

        if (optional.isPresent()) {
            BlogV2 getBlog = blogRepository.findById(blog.getBlogId()).get();
            getBlog.setBlogContent(blog.getBlogContent());
            updatedBlog = saveBlog(getBlog);
        }
        return updatedBlog;
    }
}
